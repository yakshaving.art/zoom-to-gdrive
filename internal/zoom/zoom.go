package zoom

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
	log "gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/util"

	"github.com/schollz/progressbar/v3"
	"github.com/zoom-lib-golang/zoom-lib-golang"
)

type ZoomRecordingMeeting = zoom.CloudRecordingMeeting

type ZoomRecordingFile = zoom.RecordingFile

type ZoomUser = zoom.User

var (
	days = time.Hour * 24 * -1
)

type NullDownloader struct {
}

func (n NullDownloader) DownloadZoomFiles(meetingHost string, daysAgoFrom, daysAgoTo int, downloadFolder string) error {
	host := ""
	if meetingHost != "" {
		host = " for meeting host " + meetingHost
	}
	log.Printf("SKIP downloading files%s from %d to %d days ago to %s(NULL)", host, daysAgoFrom, daysAgoTo, downloadFolder)
	return nil
}

func (n NullDownloader) DeleteDownloadedRecordings(downloadFolder string) error {
	log.Printf("SKIP deleting downloaded recordings (NULL)")
	return nil
}

type ZoomDownloader interface {
	DownloadZoomFiles(meetingHost string, daysAgoFrom, daysAgoTo int, downloadFolder string) error
	DeleteDownloadedRecordings(downloadFolder string) error
}

type ZoomClient struct {
	client *zoom.Client

	downloadedMeetings []Downloadable
}

func New(zoomAccountID, zoomClientID, zoomClientSecret string) ZoomDownloader {
	return &ZoomClient{
		client:             zoom.NewOAuth2Client(zoomAccountID, zoomClientID, zoomClientSecret),
		downloadedMeetings: make([]Downloadable, 0),
	}
}

func NewFromClient(c *zoom.Client) *ZoomClient {
	return &ZoomClient{client: c}
}

func (fe *ZoomClient) DownloadZoomFiles(meetingHost string, daysAgoFrom, daysAgoTo int, downloadFolder string) error {

	today := time.Now()
	from := today.Add(time.Duration(daysAgoFrom) * days)
	to := today.Add(time.Duration(daysAgoTo) * days)

	recordings, err := fe.FetchAllRecordings(meetingHost, from, to)
	if err != nil {
		return fmt.Errorf("could not fetch all recordings: %w", err)
	}

	for _, r := range recordings {
		files, err := fe.DownloadMeetingRecordings(r, downloadFolder)
		if err != nil {
			log.Printf("Could not download recording: %s\n", err)
			continue
		}
		for _, f := range files {
			log.Debugf("Downloaded file %s\n", f)
		}
	}
	return nil
}

func (fe *ZoomClient) FetchAllRecordings(host string, since, until time.Time) ([]Downloadable, error) {

	downloadables := make([]Downloadable, 0)

	var users []zoom.User

	if host == "" {
		u, err := fe.FetchAllUsers()
		if err != nil {
			return nil, fmt.Errorf("could not fetch zoom users: %w", err)
		}
		users = u
	} else {
		u, err := fe.GetUser(host)
		if err != nil {
			return nil, fmt.Errorf("could not fetch zoom user %s: %w", host, err)
		}
		users = u
	}

	totalMeetings := 0
	for _, u := range users {

		meetings := fe.fetchUserRecordings(u, since, until)
		totalMeetings += len(meetings)

		if len(meetings) > 0 {
			log.Debugf("Found %d recordings for user %s\n", len(meetings), u.Email)
			for _, m := range meetings {
				downloadables = append(downloadables, Downloadable{
					host:    u,
					meeting: m,
				})
			}
		}
	}

	log.Debugf("Total Recordings found: %d", totalMeetings)

	return downloadables, nil
}

func (fe *ZoomClient) GetUser(host string) ([]zoom.User, error) {
	user, err := fe.client.GetUser(zoom.GetUserOpts{
		EmailOrID: host,
	})
	if err != nil {
		return nil, fmt.Errorf("could not find zoom user %s: %w", host, err)
	}

	return []zoom.User{user}, nil
}

func (fe *ZoomClient) FetchAllUsers() ([]zoom.User, error) {
	users := make([]zoom.User, 0)

	NextPageToken := ""
	for {
		page, err := fe.client.ListUsers(zoom.ListUsersOptions{
			NextPageToken: NextPageToken,
		})
		if err != nil {
			return users, fmt.Errorf("could not fetch zoom users: %w with next page token: %q", err, NextPageToken)
		}

		users = append(users, page.Users...)

		NextPageToken = strings.TrimSpace(page.NextPageToken)
		if NextPageToken == "" {
			break
		}
	}

	sort.Slice(users, func(i, j int) bool { return users[i].Email < users[j].Email })

	return users, nil
}

func (fe *ZoomClient) everyMonth(since, until time.Time, f func(from, to string)) {
	from := since
	to := from.Add(time.Hour * 24 * 28) // Because February
	for {
		if to.After(until) {
			f(from.Format(time.DateOnly), until.Format(time.DateOnly))
			break
		} else {
			f(from.Format(time.DateOnly), to.Format(time.DateOnly))
		}

		from = to
		to = from.Add(time.Hour * 24 * 28)
	}
}

func (fe *ZoomClient) fetchUserRecordings(u zoom.User, since, until time.Time) []zoom.CloudRecordingMeeting {
	meetings := []zoom.CloudRecordingMeeting{}

	fe.everyMonth(since, until, func(from, to string) {
		NextPageToken := ""
		for {
			recodings, err := fe.client.ListAllRecordings(zoom.ListAllRecordingsOptions{
				UserID:        u.ID,
				NextPageToken: NextPageToken,
				From:          from,
				To:            to,
			})
			if err != nil {
				log.Printf("Could not list recordings for user %s: %s", u.ID, err)
			}
			if recodings.TotalRecords > 0 {
				meetings = append(meetings, recodings.Meetings...)
			}

			NextPageToken = strings.TrimSpace(recodings.NextPageToken)
			if NextPageToken == "" {
				break
			}
		}
	})

	return meetings
}

type writeWatcher struct {
	writer  io.Writer
	onWrite func(n int)
}

func (w writeWatcher) Write(p []byte) (int, error) {
	n, err := w.writer.Write(p)
	w.onWrite(n)
	return n, err
}

func (fe *ZoomClient) DownloadMeetingRecordings(d Downloadable, targetFolder string) ([]string, error) {
	downloadedFiles := make([]string, 0)
	downloadDir := d.Folder(targetFolder)
	if err := os.MkdirAll(downloadDir, 0755); err != nil {
		return nil, fmt.Errorf("could not create target download dir %s: %w", downloadDir, err)
	}

	for _, df := range d.meeting.RecordingFiles {
		targetFilename := path.Join(downloadDir, d.Filename(df))
		fileSize := int64(df.FileSize)

		if stat, err := os.Stat(targetFilename); err == nil && stat.Size() < fileSize {
			log.Printf("SKIP downloading file %s because it already exists", targetFilename)
			continue
		}

		targetFile, err := os.Create(targetFilename)
		if err != nil {
			log.Printf("Could not create download local target file %s: %s", targetFilename, err)
			continue
		}

		relpath, err := filepath.Rel(targetFolder, targetFilename)
		if err != nil {
			log.Printf("Could not find relative path for %s: %s", targetFilename, err)
			relpath = targetFilename
		}
		relpath = util.CompressPath(relpath)

		bar := progressbar.DefaultBytes(fileSize)
		barDescription := fmt.Sprintf("Downloading %s", relpath)
		bar.Describe(barDescription)

		watcher := writeWatcher{
			writer: targetFile,
			onWrite: func(n int) {
				if err := bar.Add(n); err != nil {
					logger.Printf("could not add %d to progress bar %s", n, barDescription)
				}
			},
		}

		if err := fe.client.DownloadRecordingFile(df, watcher); err != nil {
			log.Printf("Could not download target file %s: %s", relpath, err)
			continue
		}

		if err := bar.Finish(); err != nil {
			log.Printf("Could not finish progress bar for target file %s: %s", relpath, err)
		}

		downloadedFiles = append(downloadedFiles, targetFilename)
	}

	fe.downloadedMeetings = append(fe.downloadedMeetings, d)

	return downloadedFiles, nil
}

func (fe *ZoomClient) DeleteDownloadedRecordings(downloadFolder string) error {
	if len(fe.downloadedMeetings) == 0 {
		log.Printf("No recorded meetings to delete locally")
		return nil
	}

	log.Printf("Deleting %d downloaded recorded meetings", len(fe.downloadedMeetings))
	for _, m := range fe.downloadedMeetings {
		recordingFolder := m.Folder(downloadFolder)
		if err := fe.client.DeleteMeetingRecordings(zoom.DeleteMeetingRecordingsOptions{
			MeetingID: m.meeting.ID,
		}); err != nil {
			log.Printf("Failed to delete downloaded meeting %s: %s", recordingFolder, err)
		} else {
			log.Printf("Meeting %s deleted in zoom", recordingFolder)
			if err := os.RemoveAll(recordingFolder); err != nil {
				log.Printf("Could not delete locally downloaded meeting %s: %s", recordingFolder, err)
			}
		}
	}
	return nil
}

type Downloadable struct {
	host    ZoomUser
	meeting ZoomRecordingMeeting
}

func (d Downloadable) MeetingID() string {
	return d.meeting.UUID
}

func (d Downloadable) Folder(rootPath string) string {
	meetingFolder := fmt.Sprintf(
		"%s - %s",
		d.meeting.StartTime.Format(time.DateTime),
		d.meeting.Topic)

	return path.Join(rootPath, d.sanitize(d.host.Email), d.sanitize(meetingFolder))
}

func (d Downloadable) Filename(r ZoomRecordingFile) string {
	extension := strings.ToLower(r.FileExtension)
	recordingType := string(r.RecordingType)
	fileType := r.FileType

	switch fileType {
	case "":
		recordingType = "incomplete"

	case "TIMELINE":
		recordingType = strings.ToLower(r.FileType)
	}

	return d.sanitize(fmt.Sprintf("%s - %s - %s.%s",
		d.meeting.StartTime.Format(time.RFC3339), d.meeting.Topic, recordingType, extension))
}

func (d Downloadable) RecordingFiles() []ZoomRecordingFile {
	return d.meeting.RecordingFiles
}

func (d Downloadable) sanitize(source string) string {
	s := strings.ReplaceAll(source, "'", "´")
	s = strings.ReplaceAll(s, "/", "-")
	return s
}
