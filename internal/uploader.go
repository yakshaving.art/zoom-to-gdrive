package internal

import (
	"fmt"
	"os"
	"path"
	"strings"

	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
	log "gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
)

// StdOutUploader prints to the log the file that would be uploaded for debugging.
type StdOutUploader struct{}

func (u StdOutUploader) Upload(basepath, filename string, parentFolders ...string) error {
	f := path.Base(filename)
	log.Printf("Uploading %s to %s...", path.Join(basepath, filename), path.Join(path.Join(parentFolders...), f))
	return nil
}

// An object that can upload a file to a remote location
type Uploader interface {
	Upload(basepath string, filename string, parentFolders ...string) error
}

func NewFileSystemWalker(u Uploader) FileSystemWalker {
	return FileSystemWalker{u: u, d: NullDeleter{}}
}

type Deleter interface {
	Delete(filename string) error
}

type FileSystemWalker struct {
	u Uploader
	d Deleter
}

func (w FileSystemWalker) WithDeleter(d Deleter) FileSystemWalker {
	w.d = d
	return w
}

func (w FileSystemWalker) WalkPath(base string, parentFolders ...string) error {
	files, err := os.ReadDir(base)
	if err != nil {
		return fmt.Errorf("could not read folder %s: %w", base, err)
	}

	for _, f := range files {
		if f.IsDir() {
			newBase := path.Join(base, f.Name())
			newPrevious := append(parentFolders, f.Name())
			if err := w.WalkPath(newBase, newPrevious...); err != nil {
				return fmt.Errorf("failed to walk path %s: %w", newBase, err)
			}
		} else if strings.HasPrefix(f.Name(), ".") {
			logger.Debugf("Ignoring hidden file %s", f.Name())
			continue
		} else {
			if err := w.u.Upload(base, f.Name(), parentFolders...); err != nil {
				return fmt.Errorf("failed to upload file %s: %w", path.Join(path.Join(parentFolders...), f.Name()), err)
			}
		}
	}
	return nil
}

type LocalFolderDeleter struct{}

func (f LocalFolderDeleter) Delete(filename string) error {
	fldr := path.Dir(filename)

	_, err := os.Stat(fldr)
	if os.IsNotExist(err) {
		log.Printf("Folder %s does not exist\n", fldr)
		return nil
	}

	return os.RemoveAll(fldr)
}

type NullDeleter struct{}

func (f NullDeleter) Delete(filename string) error {
	log.Printf(" would have deleted file %s\n", filename)
	return nil
}
