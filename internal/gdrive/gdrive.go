package gdrive

import (
	"context"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/schollz/progressbar/v3"
	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
	log "gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/util"

	"golang.org/x/oauth2/google"

	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/option"
)

type DriveClient struct {
	driveId string
	srv     *drive.Service

	folders map[string]string
}

type DriveFileList = drive.FileList

var (
	driveFields = googleapi.Field("nextPageToken, files(id, name, mimeType, parents, driveId)")

	corpora = "drive"

	folderMimeType = "application/vnd.google-apps.folder"
)

type DriveUploader interface {
	Upload(basepath, filename string, previous ...string) error

	WithDriveID(driveId string) DriveUploader
}

func FromJSONFile(c os.File) (DriveUploader, error) {
	ctx := context.Background()
	b, err := os.ReadFile(c.Name())
	if err != nil {
		return DriveClient{}, fmt.Errorf("unable to read client secret file: %w", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, drive.DriveScope)
	if err != nil {
		return DriveClient{}, fmt.Errorf("unable to parse client secret file to config: %w", err)
	}
	client, err := getClient(config)
	if err != nil {
		return DriveClient{}, fmt.Errorf("unable to create drive client: %w", err)
	}

	drive, err := drive.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		return DriveClient{}, fmt.Errorf("unable to retrieve Drive client: %w", err)
	}

	return DriveClient{srv: drive, folders: make(map[string]string)}, nil
}

func FromGoogleDriveService(srv *drive.Service) (DriveUploader, error) {
	return DriveClient{srv: srv}, nil
}

func (d DriveClient) WithDriveID(driveId string) DriveUploader {
	d.driveId = driveId
	return d
}

// Upload at this level needs to map all the parts
func (d DriveClient) Upload(localBasepath, filename string, parentFolders ...string) error {
	log.Debugf("Uploading file %s to %+v", filename, parentFolders)

	// Build the path to previous and grab the parent ids
	driveParentID := "" // No parent ID as a start
	for _, p := range parentFolders {
		pID, err := d.FindOrCreateFolder(driveParentID, p)
		if err != nil {
			return fmt.Errorf("could not upload file %s: %w", filename, err)
		}
		driveParentID = pID
	}

	return d.uploadFileToDrive(driveParentID, localBasepath, filename, parentFolders...)
}

func (d DriveClient) ResolveParents(parent string) []string {
	if strings.TrimSpace(parent) == "" {
		return []string{d.driveId}
	}
	return []string{parent}
}

func (d DriveClient) FindFileByName(parent, filename string) (*DriveFileList, error) {
	name := path.Base(filename)

	query := fmt.Sprintf("mimeType!='%s' and name='%s' and trashed = false", folderMimeType, name)
	if strings.TrimSpace(parent) != "" {
		query = fmt.Sprintf("%s and '%s' in parents", query, parent)
	}

	r, err := d.srv.Files.List().
		Fields(driveFields).
		IncludeItemsFromAllDrives(true).
		SupportsAllDrives(true).
		Corpora(corpora).
		DriveId(d.driveId).
		Q(query).
		Do()
	if err != nil {
		return nil, fmt.Errorf("unable to list files: %w", err)
	}

	log.Debugf("Found %d files with name %s in %s/%s with query %q\n",
		len(r.Files), name, d.driveId, parent, query)

	return r, err
}

func (d DriveClient) uploadFileToDrive(remoteTargetFolder, localBasepath, filename string, parentFolders ...string) error {

	descriptionFilename := util.CompressPath(path.Join(path.Join(parentFolders...), filename))

	l, err := d.FindFileByName(remoteTargetFolder, filename)
	if err != nil {
		return fmt.Errorf("could not search files by filename %s: %w", descriptionFilename, err)
	}

	if len(l.Files) > 0 {
		log.Printf("Skipping file %s, it is already in drive", descriptionFilename)
		return nil
	}

	upload := drive.File{
		Parents: d.ResolveParents(remoteTargetFolder),
		Name:    path.Base(filename),
	}

	fullfilepath := path.Join(localBasepath, filename)
	s, err := os.Stat(fullfilepath)
	if err != nil {
		return fmt.Errorf("failed to upload file %s, could not stat: %w", fullfilepath, err)
	}
	if s.Size() == 0 {
		return fmt.Errorf("failed to upload file %s, filesize is zero", fullfilepath)
	}

	fileStats, err := os.Stat(fullfilepath)
	if err != nil {
		return fmt.Errorf("could not open file %s: %w", fullfilepath, err)
	}

	f, err := os.Open(fullfilepath)
	if err != nil {
		return fmt.Errorf("could not open file %s: %w", fullfilepath, err)
	}

	bar := progressbar.DefaultBytes(fileStats.Size())
	bar.Describe(fmt.Sprintf("Uploading file %s", descriptionFilename))
	if _, err := d.srv.Files.Create(&upload).
		SupportsAllDrives(true).
		Fields("driveId,parents,name").
		Media(f).
		ProgressUpdater(func(current, total int64) {
			if err := bar.Set64(current); err != nil {
				logger.Debugf("could not set current value of %d for %s: %s", current, descriptionFilename, err)
			}
		}).
		Do(); err != nil {

		return fmt.Errorf("failed to upload file %s: %w", descriptionFilename, err)
	}

	if err := bar.Finish(); err != nil {
		log.Printf("Failed to finish progress bar when uploading file %s: %s", descriptionFilename, err)
	}
	return nil
}

func (d DriveClient) cacheFolder(parent, name, id string) {
	key := path.Join(path.Join(d.ResolveParents(parent)...), name)
	d.folders[key] = id
}

func (d DriveClient) getCachedFolder(parent, name string) (string, bool) {
	key := path.Join(path.Join(d.ResolveParents(parent)...), name)
	value, ok := d.folders[key]
	return value, ok
}

func (d DriveClient) FindOrCreateFolder(parent, name string) (string, error) {
	log.Debugf("Finding folder %s in %s/%s\n", name, d.driveId, parent)
	if value, ok := d.getCachedFolder(parent, name); ok {
		log.Debugf("Found folder %s in the folder cache", parent)
		return value, nil
	}

	f, err := d.FindFolderByName(parent, name)
	if err != nil {
		return "", fmt.Errorf("could not find folder %s in %s/%s: %w", name, d.driveId, parent, err)
	}

	switch len(f.Files) {
	case 0:
		log.Debugf("Empty list of folders with name %s in %s/%s, creating\n", name, d.driveId, parent)
		id, err := d.CreateFolder(parent, name)
		if err != nil {
			return "", fmt.Errorf("could not create folder %s in %s/%s: %w", name, d.driveId, parent, err)
		}

		d.cacheFolder(parent, name, id)
		return id, nil

	case 1:
		id := f.Files[0].Id
		log.Debugf("Folder %s found in %s/%s with ID %s, returning\n", name, d.driveId, parent, id)

		d.cacheFolder(parent, name, id)
		return id, nil

	default: // If I find 2, I need to pick the shorter path
		shorter := f.Files[0]
		logger.Debugf("Found more than 1 instance (%d) of folder %q in parent %q, using %s as initial folder", len(f.Files), name, parent, d.getFullPath(shorter))
		for i := 1; i < len(f.Files); i++ {
			proposed := f.Files[i]
			if len(proposed.Parents) < len(shorter.Parents) {
				logger.Debugf("Swapping shorter path for %s to %s", name, d.getFullPath(proposed))
				shorter = proposed
			}
		}
		return shorter.Id, nil
	}
}
func (d DriveClient) getFullPath(f *drive.File) string {
	return path.Join(path.Join(f.Parents...), f.Name)
}

func (d DriveClient) CreateFolder(parent, name string) (string, error) {
	log.Debugf("Creating folder %s in drive %s/%s\n", name, d.driveId, parent)
	fldr := drive.File{
		Parents:  d.ResolveParents(parent),
		Name:     name,
		MimeType: folderMimeType,
	}
	// AppProperties: folder.Metadata,

	f, err := d.srv.Files.Create(&fldr).
		SupportsAllDrives(true).
		EnforceSingleParent(true).
		Fields("id").
		Do()
	if err != nil {
		return "", fmt.Errorf("could not create folder %s/%s: %w", name, d.driveId, err)
	}

	id := f.Id
	log.Printf("Folder %s created in drive\n", name)
	return id, nil
}

func (d DriveClient) FindFolderByName(parent, name string) (*DriveFileList, error) {
	query := fmt.Sprintf("mimeType='%s' and name='%s' and trashed = false", folderMimeType, name)
	if strings.TrimSpace(parent) != "" {
		query = fmt.Sprintf("%s and '%s' in parents", query, parent)
	}

	r, err := d.srv.Files.List().
		Fields(driveFields).
		IncludeItemsFromAllDrives(true).
		SupportsAllDrives(true).
		Corpora(corpora).
		DriveId(d.driveId).
		Q(query).
		Do()
	if err != nil {
		return nil, fmt.Errorf("unable to list folders: %w", err)
	}

	log.Debugf("Found %d folders with name %s in %s/%s with query %q\n",
		len(r.Files), name, d.driveId, parent, query)

	return r, err
}

func (d DriveClient) ListFilesInDrive() (*DriveFileList, error) {
	log.Debugf("searching for all files in drive %s", d.driveId)

	r, err := d.srv.Files.List().
		Fields(driveFields).
		IncludeItemsFromAllDrives(true).
		SupportsAllDrives(true).
		Corpora(corpora).
		DriveId(d.driveId).
		Do()
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve files: %w", err)
	}
	return r, err
}
