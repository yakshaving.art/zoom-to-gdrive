package main

import (
	"os"

	"github.com/alecthomas/kingpin/v2"
	log "gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
)

type Args struct {
	ZoomAccountID    string
	ZoomClientID     string
	ZoomClientSecret string

	DaysAgoFrom int
	DaysAgoTo   int

	GoogleOAuthJSONFile *os.File
	DriveId             string
	DriveFolderID       string

	DownloadFolder string

	MeetingHost string

	DontUpload   bool
	DontDownload bool

	Delete bool

	Debug bool
}

func ParseArgs() *Args {
	c := &Args{}

	app := kingpin.New("zoom-to-gdrive", "downloads recordings from zoom and uploads them to gdrive")
	app.DefaultEnvars()

	app.Flag("zoom-account-id", "Account ID in the zoom server-to-server oauth2 setup").StringVar(&c.ZoomAccountID)
	app.Flag("zoom-client-id", "Client ID in the zoom server-to-server oauth2 setup").StringVar(&c.ZoomClientID)
	app.Flag("zoom-client-secret", "Client Secret in the zoom server-to-server oauth2 setup").StringVar(&c.ZoomClientSecret)

	app.Flag("meeting-host", "Host for whom to filter meetings").StringVar(&c.MeetingHost)
	app.Flag("days-ago-from", "When pulling recordings, from how many days ago should we start querying since today").Default("30").IntVar(&c.DaysAgoFrom)
	app.Flag("days-ago-to", "When pulling recordings, until how many days ago should we query since today").Default("1").IntVar(&c.DaysAgoTo)

	app.Flag("google-oauth-json-file", "JSON file to use to authenticate against google for drive access").Default(".config.json").FileVar(&c.GoogleOAuthJSONFile)
	app.Flag("drive-id", "Drive ID to upload files to").StringVar(&c.DriveId)
	app.Flag("drive-folder-id", "Folder ID to upload files to in the drive").StringVar(&c.DriveFolderID)

	app.Flag("download-folder", "Folder in the local path to download and then upload from").Default(".").ExistingDirVar(&c.DownloadFolder)

	app.Flag("dont-download", "Skip downloading files from zoom").BoolVar(&c.DontDownload)
	app.Flag("dont-upload", "Skip uploading files to the remote storage").BoolVar(&c.DontUpload)

	app.Flag("delete", "Delete downloaded recordings from zoom after uploading them to drive").BoolVar(&c.Delete)

	app.Flag("debug", "Enable debug mode, yielding out a lot more logs").BoolVar(&c.Debug)

	_, err := app.Parse(os.Args[1:])
	if err != nil {
		log.Println(err.Error())
		app.Usage(os.Args[1:])
		os.Exit(1)
	}

	return c
}
