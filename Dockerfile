FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master

# hadolint ignore=DL3018
RUN apk --no-cache add libc6-compat

COPY zoom_to_gdrive /bin

CMD [ "/bin/zoom_to_gdrive" ]
