# Zoom To GDrive

A tool that does exactly what it says: downloads cloud-recorded meetings from Zoom, and then uploads them to a Google Drive, creating folders for each host/meeting, sorting them by meeting time.

## Usage

```sh
$ ./zoom_to_gdrive --help
usage: zoom-to-gdrive [<flags>]

downloads recordings from zoom and uploads them to gdrive


Flags:
  --[no-]help                  Show context-sensitive help (also try --help-long and --help-man). ($ZOOM_TO_GDRIVE_HELP)
  --zoom-account-id=ZOOM-ACCOUNT-ID  
                               Account ID in the zoom server-to-server oauth2 setup ($ZOOM_TO_GDRIVE_ZOOM_ACCOUNT_ID)
  --zoom-client-id=ZOOM-CLIENT-ID  
                               Client ID in the zoom server-to-server oauth2 setup ($ZOOM_TO_GDRIVE_ZOOM_CLIENT_ID)
  --zoom-client-secret=ZOOM-CLIENT-SECRET  
                               Client Secret in the zoom server-to-server oauth2 setup ($ZOOM_TO_GDRIVE_ZOOM_CLIENT_SECRET)
  --meeting-host=MEETING-HOST  Host for whom to filter meetings ($ZOOM_TO_GDRIVE_MEETING_HOST)
  --days-ago-from=30           When pulling recordings, from how many days ago should we start querying since today ($ZOOM_TO_GDRIVE_DAYS_AGO_FROM)
  --days-ago-to=1              When pulling recordings, until how many days ago should we query since today ($ZOOM_TO_GDRIVE_DAYS_AGO_TO)
  --google-oauth-json-file=.config.json  
                               JSON file to use to authenticate against google for drive access ($ZOOM_TO_GDRIVE_GOOGLE_OAUTH_JSON_FILE)
  --drive-id=DRIVE-ID          Drive ID to upload files to ($ZOOM_TO_GDRIVE_DRIVE_ID)
  --drive-folder-id=DRIVE-FOLDER-ID  
                               Folder ID to upload files to in the drive ($ZOOM_TO_GDRIVE_DRIVE_FOLDER_ID)
  --download-folder=.          Folder in the local path to download and then upload from ($ZOOM_TO_GDRIVE_DOWNLOAD_FOLDER)
  --[no-]dont-download         Skip downloading files from zoom ($ZOOM_TO_GDRIVE_DONT_DOWNLOAD)
  --[no-]dont-upload           Skip uploading files to the remote storage ($ZOOM_TO_GDRIVE_DONT_UPLOAD)
  --[no-]delete                Delete downloaded recordings from zoom after uploading them to drive ($ZOOM_TO_GDRIVE_DELETE)
  --[no-]debug                 Enable debug mode, yielding out a lot more logs ($ZOOM_TO_GDRIVE_DEBUG)
```

### What will it do

1. Connect to Google Drive using `--google-oauth-json-file`. Bail out if it fails.
   1. Unless the `dont-upload` flag is set.
1. Connect to Zoom using [Server to Server OAuth](#configuring-zoom-server-to-server-app) environment variables.
   1. Unless the `dont-download` flag is set, in which case all zoom steps are skipped.
1. Fetch all the zoom users.
   1. Unless the `--meeting-host` flag is set, in which case, only fetch that one user.
1. For all the zoom users, fetch all the meetings starting from `--days-ago-from` to `--days-ago-to`
1. One by one, download all cloud recording files to `--download-folder` with a folder structure like `<meeting-host>/<meeting start>-<meeting topic>/<meeting-topic>-<file type>.<extension>`
   1. Meetings downloaded are kept for later
1. One by one, all files downloaded are uploaded to Google Drive following the same structure, to the root folder in the `--drive-id`.
1. If `--delete`, all downloaded meetings are sent to the trash in Zoom.
1. Profit

## Configuration

### Configuring Zoom Server to Server app
- Create server-2-server OAuth credentials https://marketplace.zoom.us/

1. _Attention: You will need a Zoom _Developer account_ to create a Server-to-Server OAuth app with the required credentials
2. Create a server-to-server OAuth app, set up your app and collect your credentials (Account ID, Client ID, Client Secret). For questions on this, reference the docs on [creating a server-to-server app][server-to-server-app]. Make sure you activate the app. Follow Zoom's set up documentation.
3. Add the necessary scopes to your app. In your app's Scopes tab, add the following scopes: `account:master, account:read:admin, account:write:admin, information_barriers:read:admin, information_barriers:read:master, information_barriers:write:admin, information_barriers:write:master, meeting:master, meeting:read:admin, meeting:read:admin:sip_dialing, meeting:write:admin, meeting_token:read:admin:live_streaming, meeting_token:read:admin:local_archiving, meeting_token:read:admin:local_recording, recording:master, recording:read:admin, recording:write:admin, user:master, user:read:admin, user:write:admin.`
  
[server-to-server-app]: https://developers.zoom.us/docs/internal-apps/