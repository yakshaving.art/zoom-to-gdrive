
.PHONY: build
build: lint test
	@go build .

.PHONY: test
test:	### run all the unit tests
	@go test -v -coverprofile=coverage.out $$(go list ./... | grep -v '/vendor/') \
		&& go tool cover -func=coverage.out

.PHONY: lint
lint:	### run all the lints
	@golangci-lint run
