package logger

import (
	"log"
	"strings"
)

var (
	DebugMode = false
)

func Printf(format string, v ...any) {
	if !strings.HasSuffix(format, "\n") {
		format = format + "\n"
	}
	log.Printf(format, v...)
}

func Println(v ...any) {
	log.Println(v...)
}

func Debugf(format string, v ...any) {
	if DebugMode {
		Printf(format, v...)
	}
}
