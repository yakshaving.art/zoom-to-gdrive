package util_test

import (
	"testing"

	"gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/util"
)

func TestCompressPath(t *testing.T) {

	if util.CompressPath("first/second/third") != "first/.../third" {
		t.Errorf("one %s", util.CompressPath("first/second/third"))
	}

	if util.CompressPath("first/2/third") != "first/2/third" {
		t.Errorf("two %s", util.CompressPath("first/2/third"))
	}

	if util.CompressPath("first/second") != "first/second" {
		t.Errorf("three")
	}

	if util.CompressPath("first") != "first" {
		t.Errorf("fourth")
	}

}
