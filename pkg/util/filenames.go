package util

import (
	"path"
	"path/filepath"
	"strings"
)

func CompressPath(s string) string {
	paths := strings.Split(s, string(filepath.Separator))
	if len(paths) < 3 {
		return s
	}

	first := paths[0]
	last := paths[len(paths)-1]

	proposed := path.Join(first, "...", last)
	if len(proposed) > len(s) {
		return s
	}

	return proposed
}
