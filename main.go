package main

import (
	"os"

	"gitlab.com/yakshaving.art/zoom_to_gdrive/internal"
	"gitlab.com/yakshaving.art/zoom_to_gdrive/internal/gdrive"
	"gitlab.com/yakshaving.art/zoom_to_gdrive/internal/zoom"
	log "gitlab.com/yakshaving.art/zoom_to_gdrive/pkg/logger"
)

const (
	ExitCantDownload                   = 2
	ExitCantCreateDriveClient          = 3
	ExitCantUploadFiles                = 4
	ExitCantDeleteDownloadedRecordings = 5
)

func main() {
	c := ParseArgs()

	log.DebugMode = c.Debug

	var uploader internal.Uploader
	if c.DontUpload {
		uploader = internal.StdOutUploader{}
	} else {
		driveUploader, err := gdrive.FromJSONFile(*c.GoogleOAuthJSONFile)
		if err != nil {
			log.Printf("Could not create Google Drive client: %s\n", err)
			os.Exit(ExitCantCreateDriveClient)
		}
		uploader = driveUploader.WithDriveID(c.DriveId)
	}

	var downloader zoom.ZoomDownloader
	if c.DontDownload {
		downloader = zoom.NullDownloader{}
	} else {
		downloader = zoom.New(c.ZoomAccountID, c.ZoomClientID, c.ZoomClientSecret)
	}

	log.Debugf("Downloading zoom files to folder " + c.DownloadFolder)
	if err := downloader.DownloadZoomFiles(c.MeetingHost, c.DaysAgoFrom, c.DaysAgoTo, c.DownloadFolder); err != nil {
		log.Printf("Could not download zoom files: %s\n", err)
		os.Exit(ExitCantDownload)
	}

	w := internal.NewFileSystemWalker(uploader)

	log.Debugf("Uploading files from folder " + c.DownloadFolder)
	if err := w.WalkPath(c.DownloadFolder); err != nil {
		log.Printf("  Failed while walking folder %s: %s", c.DownloadFolder, err)
		os.Exit(ExitCantUploadFiles)
	}

	if c.Delete {
		log.Debugf("Deleting downloaded zoom meetings to folder " + c.DownloadFolder)
		if err := downloader.DeleteDownloadedRecordings(c.DownloadFolder); err != nil {
			log.Printf("Could not delete downloaded recordings: %s", err)
			os.Exit(ExitCantDeleteDownloadedRecordings)
		}
	}
}
